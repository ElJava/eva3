from django.contrib import admin

# Register your models here.
from appgestion.models import Usuarios, CasasPrefabricadas

admin.site.register(Usuarios)
admin.site.register(CasasPrefabricadas)
