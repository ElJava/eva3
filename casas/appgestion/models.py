from django.db import models

# Create your models here.


class CasasPrefabricadas(models.Model):
    tamano=models.CharField(max_length=10)
    banos=models.IntegerField()
    habitaciones=models.IntegerField()
    precio=models.IntegerField()
    tipoMadera=models.CharField(max_length=50)


class Usuarios(models.Model):
    nombre=models.CharField(max_length=50)
    apellido=models.CharField(max_length=50)
    telefono=models.CharField(max_length=10)
    correo=models.CharField(max_length=70)
    direccion=models.CharField(max_length=50)



