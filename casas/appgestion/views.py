from django.shortcuts import render
from django.http import HttpResponse
from appgestion.models import CasasPrefabricadas, Usuarios


# Create your views here.
from django.shortcuts import render
from django.http import HttpResponse
from appgestion.models import CasasPrefabricadas, Usuarios

# Create your views here.
#----------------DEF PARA BUSCAR USUARIOS---------------------------------
def busqueda_usuarios(request):
    return render(request, "busqueda_casas.html")

def buscar_nombre(request):
    #si txt_casa viene con datos si devuelve true
    if request.GET["txt_user"]:
        nombre_recibida= request.GET["txt_user"]
        #comandos usados en la shell
        user=Usuarios.objects.filter(nombre__contains=nombre_recibida)
        return render(request,"resultados_users.html",{"user":user,"user_consultada":nombre_recibida})
    else:
        mensaje="Debe ingresar un usuario correcto"
    return render(request,"error.html",{"error_msg":mensaje})

def buscar_apellidos(request):
    #si txt_casa viene con datos si devuelve true
    if request.GET["txt_user"]:
        apellido_recibido= request.GET["txt_user"]
        #comandos usados en la shell
        user=Usuarios.objects.filter(apellido__contains=apellido_recibido)
        return render(request,"resultados_users.html",{"user":user,"user_consultada":apellido_recibido})
    else:
        mensaje="Debe ingresar un apellido correcto"
    return render(request,"error.html",{"error_msg":mensaje})

def buscar_correo(request):
    #si txt_casa viene con datos si devuelve true
    if request.GET["txt_user"]:
        correo_recibido= request.GET["txt_user"]
        #comandos usados en la shell
        user=Usuarios.objects.filter(correo__contains=correo_recibido)
        return render(request,"resultados_users.html",{"user":user,"user_consultada":correo_recibido})
    else:
        mensaje="Debe ingresar un correo correcto"
    return render(request,"error.html",{"error_msg":mensaje})

#----------------DEF PARA BUSCAR CASAS---------------------------------

def busqueda_casas(request):
    return render(request, "busqueda_casas.html")

def buscar_casas_material(request):
    #si txt_casa viene con datos si devuelve true
    if request.GET["txt_casa"]:
        casa_recibida= request.GET["txt_casa"]
        #comandos usados en la shell
        casa=CasasPrefabricadas.objects.filter(tipoMadera__contains=casa_recibida)
        return render(request,"resultados_casas.html",{"casa":casa,"casa_consultada":casa_recibida})
    else:
        mensaje="Debe ingresar un material a buscar"
    return render(request,"error.html",{"error_msg":mensaje})

def buscar_casas_bano(request):
    #si txt_casa viene con datos si devuelve true
    if request.GET["txt_casa"]:
        casa_recibida= request.GET["txt_casa"]
        #comandos usados en la shell
        casa=CasasPrefabricadas.objects.filter(banos__contains=casa_recibida)
        return render(request,"resultados_casas.html",{"casa":casa,"casa_consultada":casa_recibida})
    else:
        mensaje="Debe ingresar un numero de baño a buscar"
    return render(request,"error.html",{"error_msg":mensaje})

def buscar_casas_habitacion(request):
    #si txt_casa viene con datos si devuelve true
    if request.GET["txt_casa"]:
        casa_recibida= request.GET["txt_casa"]
        #comandos usados en la shell
        casa=CasasPrefabricadas.objects.filter(habitaciones__contains=casa_recibida)
        return render(request,"resultados_casas.html",{"casa":casa,"casa_consultada":casa_recibida})
    else:
        mensaje="Debe ingresar un numero de habitaciones"
    return render(request,"error.html",{"error_msg":mensaje})

# ------------------- DEF ELIMINAR---------------------------------

def eliminar_usuario(request):
    if request.GET["txt_id_usuario"]: #txt_id si trea un dato se ejecuta if como true
        id_recibido=request.GET["txt_id_usuario"]
        client=Usuarios.objects.filter(id=id_recibido)
        if client:
            cli=Usuarios.objects.get(id=id_recibido)
            cli.delete()
            mensaje="Usuario eliminado"
        else:
            mensaje="No se encontro usuario para eliminar"
    else:
        mensaje="Debe ingresar un id"
    return render(request,"error.html",{"error_msg":mensaje})

def eliminar_casa(request):
    if request.GET["txt_id_casa"]: #txt_id si trea un dato se ejecuta if como true
        id_recibido=request.GET["txt_id_casa"]
        home = CasasPrefabricadas.objects.filter(id=id_recibido)
        if home:
            casa = CasasPrefabricadas.objects.get(id=id_recibido)
            casa.delete()
            mensaje="Casa prefabricada eliminada"
        else:
            mensaje="No se encontro la casa para eliminar"
    else:
        mensaje="Debe ingresar un id"
    return render(request,"error.html",{"error_msg":mensaje})

#------------------DEF PARA AGREGAR------------------------------------

def ingresar_casa(request):
    tamano=request.GET["txt_tamano"]
    banos=request.GET["txt_banos"]
    habitaciones=request.GET["txt_habitaciones"]
    precio=request.GET["txt_precio"]
    tipoMadera=request.GET["txt_tipoMadera"]
    if len(banos) >0 and len(habitaciones) >0 and len(precio) >0 and len(tamano)>0 and len(tipoMadera) >0:
        if int(banos) >0 and int(habitaciones) >0 and int(precio) >0:
            ca=CasasPrefabricadas(tamano=tamano, banos=int(banos), habitaciones=int(habitaciones), precio=int(precio), tipoMadera=tipoMadera)
            ca.save()
            mensaje="Casa Prefabicada ingresada"
        else:
            mensaje = "Error al ingresar casa. Las cantidades deben ser positivas"
    else:
        mensaje="Casa Prefabicada No ingresada. Faltan datos por ingresar..."
    return render(request,"error.html",{"error_msg":mensaje})

def ingresar_usuario(request):
    nombre=request.GET["txt_nombre"]
    apellido=request.GET["txt_apellido"]
    telefono=request.GET["txt_telefono"]
    correo=request.GET["txt_correo"]
    direccion=request.GET["txt_direccion"]
    if len(telefono)>0 and len(telefono)<10 and len(direccion) >0 and len(nombre) >0 and len(apellido) >0 and len(correo) >0:
        us=Usuarios(nombre=nombre, apellido=apellido, telefono=telefono, correo=correo, direccion=direccion)
        us.save()
        mensaje="Usuario ingresado"
    else:
        mensaje="Usuario No ingresado. Faltan datos por ingresar..."
    return render(request,"error.html",{"error_msg":mensaje})

#------------------DEF PARA PAGINAS WEB---------------------------------

def delete(request):
    return render(request, "eliminar.html")

def create(request):
    return render(request, "agregar.html")

def gallery(request):
    return render(request, "galeria.html")

def index(request):
    return render(request, "index.html")

def contact(request):
    return render(request, "contacto.html")

def somos(request):
    return render(request, "quienes.html")
