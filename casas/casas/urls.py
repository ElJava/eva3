"""casas URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from appgestion.views import *
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('busqueda_casas/', busqueda_casas, name="busqueda"),
    path('busqueda_casas/', busqueda_usuarios, name="busqueda"),
    path('buscart/', buscar_casas_material),
    path('buscarp/', buscar_casas_habitacion),
    path('buscarb/', buscar_casas_bano),

    path('buscarn/', buscar_nombre),
    path('buscara/', buscar_apellidos),
    path('buscarco/', buscar_correo),

    path('eliminar/',delete,name="eliminar"),
    path('eliminaru/',eliminar_usuario),
    path('eliminarc/',eliminar_casa),

    path('agregar/',create,name="agregar"),
    path('agregarc/',ingresar_casa),
    path('agregaru/',ingresar_usuario),

    path('galeria/',gallery,name="galeria"),
    path('',index,name="index"),
    path('contacto/',contact,name="contacto"),
    path('quienes/',somos,name="quienes"),
]

